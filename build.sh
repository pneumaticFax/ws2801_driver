#!/bin/bash

set -o pipefail

basedir="/path/to/base/of/project"
psmdir=`readlink -f "$basedir/sw/picoblaze/"`

csicondir=`readlink -f "$basedir/gen/csicon/"`
csiladir=`readlink -f "$basedir/gen/csila"`

starttime=`date`
startsecs=`date +"%s"`

printsep () {
    echo ""
    for i in `seq $(stty size | cut -d' ' -f2)`; do echo -n "#"; done
    echo "$1"
    for i in `seq $(stty size | cut -d' ' -f2)`; do echo -n "#"; done
    echo ""
}

printsep BUILD SCRIPT
echo "basedir: $basedir"
echo "psmdir: $psmdir"
echo "csicondir: $csicondir"
echo "csiladir : $csiladir"
#echo "Press enter to begin"
#read


########################################################
printsep KCPSM6

cd $psmdir
wine kcpsm6.exe programrom.psm || exit 1


########################################################
#printsep COREGEN : chipscope_icon
#cd $csicondir
#coregen -b chipscope_icon.xco -p coregen.cgp \
#    | tee $basedir/xil/reports/coregen/chipscope_icon.log || exit 1
#
#printsep COREGEN : chipscope_ila
#cd $csiladir
#coregen -b chipscope_ila.xco -p coregen.cgp \
#    | tee $basedir/xil/reports/coregen/chipscope_ila.log || exit 1


########################################################
cd $basedir
printsep XST

xst -intstyle silent \
    -ifn "proj/projname.xst" \
    -ofn "xil/reports/xst/projname.syr" \
    | tee xil/reports/xst/cmd.log || exit 1


########################################################
printsep NGDBUILD

ngdbuild -intstyle silent \
        -sd $csiladir \
        -sd $csicondir \
        -dd xil/ngdbuild/tmp \
        -nt timestamp \
        -uc proj/ml605.ucf \
        -p xc6vlx240t-ff1156-1 \
        xil/xst/projname.ngc \
        xil/ngdbuild/projname.ngd \
        | tee xil/reports/ngdbuild/cmd.log || exit 1

    # copy build log
    cp xil/ngdbuild/projname.bld xil/reports/ngdbuild

########################################################
printsep MAP

map -intstyle silent \
    -p xc6vlx240t-ff1156-1 \
    -w \
    -logic_opt off \
    -ol std \
    -t 1 \
    -xt 0 \
    -register_duplication off \
    -r 4 \
    -global_opt off \
    -mt 2 \
    -ir off \
    -pr b \
    -lc off \
    -power off \
    -timing \
    -smartguide xil/par/projname.ncd \
    -o xil/map/projname_map.ncd \
    xil/ngdbuild/projname.ngd \
    xil/map/projname.pcf \
    | tee xil/reports/map/cmd.log || exit 1

# copy build log
cp xil/map/projname_map.mrp xil/reports/map

# move xrpt
mv projname_map.xrpt xil/map/


########################################################
printsep PAR

par -intstyle silent \
    -w \
    -ol std \
    -mt 2 \
    xil/map/projname_map.ncd \
    xil/par/projname.ncd \
    xil/map/projname.pcf \
    | tee xil/reports/par/cmd.log || exit 1

# copy build log
cp xil/par/projname.par xil/reports/par

# move xrpt
mv projname_par.xrpt xil/par/


########################################################
printsep TRCE

trce -intstyle silent \
    -v 3 \
    -s 1 \
    -n 3 \
    -fastpaths \
    -xml xil/reports/trce/projname.twx \
    -o xil/reports/trce/projname.twr \
    xil/par/projname.ncd \
    xil/map/projname.pcf \
    | tee xil/reports/trce/cmd.log || exit 1


########################################################
printsep BITGEN

bitgen -intstyle silent \
    -d \
    -g GWE_cycle:done \
    -g GTS_cycle:done \
    -g DriveDone:Yes \
    -g StartupClk:Cclk \
    -f proj/projname.ut \
    xil/par/projname.ncd \
    xil/bitgen/projname.bit \
    xil/map/projname.pcf \
    | tee xil/reports/bitgen/cmd.log || exit 1

# copy build log
cp xil/bitgen/projname.bgn xil/reports/bitgen


########################################################
printsep IMPACT

impact -batch proj/impactcmds \
    | tee xil/reports/impact/cmd.log # || exit 1

# move log
mv _impactbatch.log xil/reports/impact/impactbatch.log
mv xilinx_device_details.xml xil/reports/impact


########################################################
printsep CLEANUP

mv xlnx_auto_0_xdb xil
mv _xmsgs xil/reports/xmsgs


########################################################
printsep SUMMARY

stopsecs=`date +"%s"`
timediff=$(($stopsecs-$startsecs))

echo "Start : $starttime"
echo "Finish: `date`"
echo "Duration: $timediff secs (about $(($timediff/60)) minutes)"

